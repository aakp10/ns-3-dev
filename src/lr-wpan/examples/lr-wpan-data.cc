/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 The Boeing Company
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author:  Tom Henderson <thomas.r.henderson@boeing.com>
 */

/*
 * Try to send data end-to-end through a LrWpanMac <-> LrWpanPhy <->
 * SpectrumChannel <-> LrWpanPhy <-> LrWpanMac chain
 *
 * Trace Phy state changes, and Mac DataIndication and DataConfirm events
 * to stdout
 */
#include <ns3/log.h>
#include <ns3/core-module.h>
#include <ns3/lr-wpan-module.h>
#include <ns3/propagation-loss-model.h>
#include <ns3/propagation-delay-model.h>
#include <ns3/simulator.h>
#include <ns3/single-model-spectrum-channel.h>
#include <ns3/constant-position-mobility-model.h>
#include <ns3/packet.h>

#include <iostream>
#include <string>

using namespace ns3;

static void DataIndication (McpsDataIndicationParams params, Ptr<Packet> p, uint16_t node)
{
  std::string escape;
  TimeWithUnit now = Simulator::Now ().As (Time::S);

  if (node == 0)
    {
      escape = "\e[96m";
    }
  else if (node == 1)
    {
      escape = "\e[92m";
    }
  else if (node == 2)
    {
      escape = "\e[91m";
    }

  if (params.m_srcAddrMode == SHORT_ADDR)
    {
      NS_LOG_UNCOND (now << escape << " Node " << node << ": Received packet of size " << p->GetSize () << " from " << params.m_srcAddr << " to " << params.m_dstAddr << "\e[0m");
    }
  else if (params.m_srcAddrMode == EXT_ADDR)
    {
      NS_LOG_UNCOND (now << escape << " Node " << node << ": Received packet of size " << p->GetSize () << " from " << params.m_srcAddr << " to " << params.m_dstAddr << "\e[0m");
    }
  else
    {
      NS_LOG_UNCOND (now << escape << " Node " << node << ": Received packet of size " << p->GetSize () << " but address is of an unknown type" << "\e[0m");
    }
}

static void DataIndication0 (McpsDataIndicationParams params, Ptr<Packet> p)
{
  DataIndication (params, p, 0);
}

static void DataIndication1 (McpsDataIndicationParams params, Ptr<Packet> p)
{
  DataIndication (params, p, 1);
}

static void DataIndication2 (McpsDataIndicationParams params, Ptr<Packet> p)
{
  DataIndication (params, p, 2);
}

static void DataConfirm (McpsDataConfirmParams params, uint16_t node)
{
  std::string escape;
  TimeWithUnit now = Simulator::Now ().As (Time::S);

  if (node == 0)
    {
      escape = "\e[96m";
    }
  else if (node == 1)
    {
      escape = "\e[92m";
    }
  else if (node == 2)
    {
      escape = "\e[91m";
    }

  NS_LOG_UNCOND (now << escape << " Node " << node << ": LrWpanMcpsDataConfirmStatus = " << params.m_status << "\e[0m");
}

static void DataConfirm0 (McpsDataConfirmParams params)
{
  DataConfirm (params, 0);
}

static void DataConfirm1 (McpsDataConfirmParams params)
{
  DataConfirm (params, 1);
}

static void DataConfirm2 (McpsDataConfirmParams params)
{
  DataConfirm (params, 2);
}

static void StateChangeNotification (std::string context, Time now, LrWpanPhyEnumeration oldState, LrWpanPhyEnumeration newState)
{
  NS_LOG_UNCOND (context << " state change at " << now.GetSeconds ()
                         << " from " << LrWpanHelper::LrWpanPhyEnumerationPrinter (oldState)
                         << " to " << LrWpanHelper::LrWpanPhyEnumerationPrinter (newState));
}

int main (int argc, char *argv[])
{
  bool verbose = false;
  bool stateChange = true;
  bool extended = false;
  std::string macType = "NullMac";

  CommandLine cmd;

  cmd.AddValue ("verbose", "turn on all log components", verbose);
  cmd.AddValue ("printStateChange", "turn on MAC State Change messages", stateChange);
  cmd.AddValue ("extended", "use extended addressing", extended);
  cmd.AddValue ("MAC", "type of MAC to use (NullMac or ContikiMac)", macType);

  cmd.Parse (argc, argv);

  LrWpanHelper lrWpanHelper;
  if (verbose)
    {
      lrWpanHelper.EnableLogComponents ();
    }

  // Enable calculation of FCS in the trailers. Only necessary when interacting with real devices or wireshark.
  // GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

  // Create 3 nodes, and a NetDevice for each one
  Ptr<Node> n0 = CreateObject <Node> ();
  Ptr<Node> n1 = CreateObject <Node> ();
  Ptr<Node> n2 = CreateObject <Node> ();

  if (macType == "NullMac")
    Config::SetDefault ("ns3::LrWpanNetDevice::MacType", StringValue ("NullMac"));
  else if (macType == "ContikiMac")
    Config::SetDefault ("ns3::LrWpanNetDevice::MacType", StringValue ("ContikiMac"));
  else
    NS_ABORT_MSG ( macType << " is not a valid MAC type");

  Ptr<LrWpanNetDevice> dev0 = CreateObject<LrWpanNetDevice> ();
  Ptr<LrWpanNetDevice> dev1 = CreateObject<LrWpanNetDevice> ();
  Ptr<LrWpanNetDevice> dev2 = CreateObject<LrWpanNetDevice> ();

  if (!extended)
    {
      dev0->SetAddress (Mac16Address ("00:01"));
      dev1->SetAddress (Mac16Address ("00:02"));
      dev2->SetAddress (Mac16Address ("00:03"));
    }
  else
    {
      Ptr<LrWpanMac> mac0 = dev0->GetMac();
      Ptr<LrWpanMac> mac1 = dev1->GetMac();
      Ptr<LrWpanMac> mac2 = dev2->GetMac();
      mac0->SetExtendedAddress (Mac64Address ("00:00:00:00:00:00:00:01"));
      mac1->SetExtendedAddress (Mac64Address ("00:00:00:00:00:00:00:02"));
      mac2->SetExtendedAddress (Mac64Address ("00:00:00:00:00:00:00:03"));
    }

  // Each device must be attached to the same channel
  Ptr<SingleModelSpectrumChannel> channel = CreateObject<SingleModelSpectrumChannel> ();
  Ptr<LogDistancePropagationLossModel> propModel = CreateObject<LogDistancePropagationLossModel> ();
  Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel> ();
  channel->AddPropagationLossModel (propModel);
  channel->SetPropagationDelayModel (delayModel);

  dev0->SetChannel (channel);
  dev1->SetChannel (channel);
  dev2->SetChannel (channel);

  // To complete configuration, a LrWpanNetDevice must be added to a node
  n0->AddDevice (dev0);
  n1->AddDevice (dev1);
  n2->AddDevice (dev2);

  // Trace state changes in the phy
  if (stateChange)
    {
      dev0->GetPhy ()->TraceConnect ("TrxState", std::string ("phy0"), MakeCallback (&StateChangeNotification));
      dev1->GetPhy ()->TraceConnect ("TrxState", std::string ("phy1"), MakeCallback (&StateChangeNotification));
      dev2->GetPhy ()->TraceConnect ("TrxState", std::string ("phy2"), MakeCallback (&StateChangeNotification));
    }

  Ptr<ConstantPositionMobilityModel> sender0Mobility = CreateObject<ConstantPositionMobilityModel> ();
  sender0Mobility->SetPosition (Vector (0,0,0));
  dev0->GetPhy ()->SetMobility (sender0Mobility);

  // Configure position 10 m distance
  Ptr<ConstantPositionMobilityModel> sender1Mobility = CreateObject<ConstantPositionMobilityModel> ();
  sender1Mobility->SetPosition (Vector (0,10,0));
  dev1->GetPhy ()->SetMobility (sender1Mobility);

  // Configure position 10 m distance
  Ptr<ConstantPositionMobilityModel> sender2Mobility = CreateObject<ConstantPositionMobilityModel> ();
  sender2Mobility->SetPosition (Vector (5*sqrt(3),5,0));
  dev2->GetPhy ()->SetMobility (sender2Mobility);

  McpsDataConfirmCallback cb0;
  cb0 = MakeCallback (&DataConfirm0);
  dev0->GetMac ()->SetMcpsDataConfirmCallback (cb0);

  McpsDataIndicationCallback cb1;
  cb1 = MakeCallback (&DataIndication0);
  dev0->GetMac ()->SetMcpsDataIndicationCallback (cb1);

  McpsDataConfirmCallback cb2;
  cb2 = MakeCallback (&DataConfirm1);
  dev1->GetMac ()->SetMcpsDataConfirmCallback (cb2);

  McpsDataIndicationCallback cb3;
  cb3 = MakeCallback (&DataIndication1);
  dev1->GetMac ()->SetMcpsDataIndicationCallback (cb3);

  McpsDataConfirmCallback cb4;
  cb4 = MakeCallback (&DataConfirm2);
  dev2->GetMac ()->SetMcpsDataConfirmCallback (cb4);

  McpsDataIndicationCallback cb5;
  cb5 = MakeCallback (&DataIndication2);
  dev2->GetMac ()->SetMcpsDataIndicationCallback (cb5);

  // Tracing
  lrWpanHelper.EnablePcapAll (std::string ("lr-wpan-data"), true);
  AsciiTraceHelper ascii;
  Ptr<OutputStreamWrapper> stream = ascii.CreateFileStream ("lr-wpan-data.tr");
  lrWpanHelper.EnableAsciiAll (stream);

  // The below should trigger two callbacks when end-to-end data is working
  // 1) DataConfirm callback is called
  // 2) DataIndication callback is called with value of 50
  Ptr<Packet> p0 = Create<Packet> (50);  // 50 bytes of dummy data
  McpsDataRequestParams params;
  params.m_dstPanId = 0;
  if (!extended)
    {
      params.m_srcAddrMode = SHORT_ADDR;
      params.m_dstAddrMode = SHORT_ADDR;
      params.m_dstAddr = Mac16Address ("00:02");
    }
  else
    {
      params.m_srcAddrMode = EXT_ADDR;
      params.m_dstAddrMode = EXT_ADDR;
      params.m_dstExtAddr = Mac64Address ("00:00:00:00:00:00:00:02");
    }
  params.m_msduHandle = 0;
  params.m_txOptions = TX_OPTION_ACK;
  Simulator::ScheduleWithContext (1, Seconds (0.0),
                                  &LrWpanMac::McpsDataRequest,
                                  dev0->GetMac (), params, p0);

  // Send a packet back at time 2 seconds
  Ptr<Packet> p2 = Create<Packet> (60);  // 60 bytes of dummy data
  if (!extended)
    {
      params.m_dstAddr = Mac16Address ("00:01");
    }
  else
    {
      params.m_dstExtAddr = Mac64Address ("00:00:00:00:00:00:00:01");
    }
  Simulator::ScheduleWithContext (2, Seconds (2.0),
                                  &LrWpanMac::McpsDataRequest,
                                  dev1->GetMac (), params, p2);

  if (!extended)
    {
      // Send a broadcast packet at time 3 seconds
      Ptr<Packet> p3 = Create<Packet> (40);  // 60 bytes of dummy data
      params.m_txOptions = TX_OPTION_NONE;
      params.m_dstAddr = Mac16Address ("ff:ff");
      Simulator::ScheduleWithContext (3, Seconds (3.0),
                                      &LrWpanMac::McpsDataRequest,
                                      dev2->GetMac (), params, p3);
    }

  Simulator::Run ();

  Simulator::Destroy ();
  return 0;
}
